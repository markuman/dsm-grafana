#!/usr/bin/python
import json
import mariasql
import sys
import select
import os

##################
# read docker secrets
# and env variables

with open('/run/secrets/mariadb_passwords', 'r') as file:
    mariadb_password = file.read().replace('\n', '')

maridb_user = os.environ['MARIADB_USER']
maridb_database = os.environ['MARIADB_DATABASE']
maridb_port = int(os.environ['MARIADB_PORT'])
mariadb = os.environ['MARIADB']

#######################
# create connection
db = mariasql.MariaSQL(host=mariadb, user=maridb_user, password=mariadb_password, port=maridb_port, db=maridb_database)

if __name__ == "__main__":
    if select.select([sys.stdin, ], [], [], 30)[0]:
        try:
            JSON = json.loads(sys.stdin.read())
        except:
            print("ERROR: stdin data is not a valid json object")
            sys.exit(1)
        for data in JSON:
            db.insert_on_duplicate('manager_status', data)