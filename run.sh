#!/bin/bash

# fail fast!
docker node ls --format '{{json .}}' | jq --slurp | dsm2mariadb.py
while true;
do
    sleep 10
    docker node ls --format '{{json .}}' | jq --slurp | dsm2mariadb.py
done
