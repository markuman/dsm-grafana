FROM alpine:3.10

RUN apk update && apk add python3 docker bash jq
RUN pip3 install mariasql
RUN ln -s /usr/bin/python3 /usr/bin/python

COPY run.sh /usr/local/bin/
COPY dsm2mariadb.py /usr/local/bin/

CMD /usr/local/bin/run.sh