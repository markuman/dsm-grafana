# Docker Swarm Mode Metrics

![dsm-grafana](https://git.osuv.de/m/dsm-grafana/raw/branch/master/dsm_grafana.png)

## Requirements

* Grafana
* MariaDB >= 10.3
* Docker Swarm Mode

## Data Structure

On MariaDB we use the "versioned table" feature.  
That means:  

  1. history is available for the nodes status
  2. only changes on node status waste storage - doesn't matter which intervall you're using.


```sql
CREATE TABLE manager_status (
	Availability char(16),
	EngineVersion char(16),
	Hostname char(16),
	ID char(32),
	ManagerStatus char(16),
	`Self` bool,
	Status char(16),
	TLSStatus char(16),
	PRIMARY KEY (`Hostname`),
    INDEX `idx_manager_status` (`ManagerStatus`),
    INDEX `idx_status` (`Status`),
    INDEX `idx_availability` (`Availability`)
) WITH SYSTEM VERSIONING
  PARTITION BY SYSTEM_TIME (
    PARTITION p_hist HISTORY,
    PARTITION p_cur CURRENT
  );
```

## Deployment

Requirements:
  * Mariadb >= 10.3 is deployed in the same network (in this example, also in the `core`  network).
  * The database is manually created
  * `mariadb_passwords` is a docker secret

```
docker service create \
    --name dsm_metrics \
    --network core \
    -e MARIADB_USER=root \
    -e MARIADB_DATABASE=dsm \
    -e MARIADB_PORT=3306 \
    -e MARIADB=mariadb \
    --secret mariadb_passwords \
    --mount type=bind,source=/run/docker.sock,destination=/var/run/docker.sock \
    --constraint node.role==manager \
    registry.gitlab.com/markuman/dsm-grafana:1
```

# TODO

* Ansible Playbooks
  * Docker Service Deployment
  * Grafana Dashboard
* Docker HealthCheck
  * Deletes history data > n days?